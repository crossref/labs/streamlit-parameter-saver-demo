from streamlit.testing.v1 import AppTest
from streamlit_parameter_saver_demo.settings import DEFAULT_STATE, ALLOWED_PARAMS


def test_smoke():
    at = AppTest("app.py", default_timeout=60).run()
    assert not at.exception


def test_default_session_state():
    at = AppTest("app.py", default_timeout=60).run()
    assert all(at.session_state[k] == v for k, v in DEFAULT_STATE.items())


def test_restore_params():
    at = AppTest("app.py", default_timeout=60)
    at.query_params["color"] = "red"
    at.query_params["size"] = "m"
    at.query_params["letters"] = ["c"]
    at.run()
    assert not at.exception
    assert at.session_state["color"] == "red"
    assert at.session_state["size"] == "m"
    assert at.session_state["letters"] == ["c"]


def test_cannot_restore_invalid_params():
    at = AppTest("app.py", default_timeout=60)
    at.query_params["password"] = "red"
    at.run()
    assert not at.exception
    assert at.session_state["password"] is None


def test_select_new_items():
    at = AppTest("app.py", default_timeout=60)
    at.run()
    assert not at.exception
    assert at.session_state["letters"] == []
    at.multiselect(key="_letters").select("c")
    at.run()
    assert at.session_state["letters"] == ["c"]
    at.multiselect(key="_letters").select("a")
    at.run()
    assert at.session_state["letters"] == ["c", "a"]
