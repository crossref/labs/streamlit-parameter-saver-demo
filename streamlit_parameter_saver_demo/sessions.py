import logging
import streamlit as st
from streamlit_parameter_saver_demo.settings import DEFAULT_STATE, ALLOWED_PARAMS

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


def cast_param_value(param, value):
    """Cast param value to correct type"""
    if isinstance(DEFAULT_STATE[param], list):
        return value
    if isinstance(DEFAULT_STATE[param], bool):
        # Streamlit treats all params as lists, even if they are single values
        # So we need to convert them back to single values when appropriate
        # we use the values in DEFAULT_SHARED_STATE to determine if a param
        # should be a list or a single value

        # A list of truthy values that we will interpret as True with boolean params.
        # Anything else will be interpreted as False
        truthy_values = {
            "true",
            "1",
            "t",
            "y",
            "yes",
            "yeah",
            "yup",
            "certainly",
            "uh-huh",
        }

        return value[0].lower() in truthy_values

    return f"{value[0]}"


def params_to_state():
    for k in st.query_params:
        if k in ALLOWED_PARAMS:
            st.session_state[k] = cast_param_value(k, st.query_params.get_all(k))


def state_to_params():
    """Copy session state to URL"""
    for k, v in st.session_state.items():
        if k in ALLOWED_PARAMS:
            st.query_params[k] = v


def new_session():
    """Initialize session state"""
    st.session_state.update(DEFAULT_STATE)


# NB that streamlit session state is weird in that, if a widget is not
# used, it will purged from session state. This occurs frequently in the case
# of multi-page apps, where a widget is used on one page, but not on another.
# or in apps that show/hide widgets based on user input. These functions
# are designed to ensure that the session state is consistent across pages
# they work by shadowing the ephemeral widget key (e.g. _color) with the
# persistent session state key (e.g. color), you can then use the restore_widget_value
# to restore the widget value from the session state, and persist_widget_value to
# save the widget value to the session state. This ensures that the session state
# is always consistent with the widgets, even when the ephemeral widget keys are purged.


def persist_widget_value(key):
    st.session_state[key] = st.session_state[f"_{key}"]


def restore_widget_value(key):
    st.session_state[f"_{key}"] = st.session_state[key]


def init():
    logger.info("*** init()")
    if "inited" in st.session_state:
        logger.info("*** Continuing session")
    else:
        logger.info("*** First time run")
        # This is the first time the app is run
        # set everything to the default state
        # and then override defaults with any
        # **allowed* params in the URL
        new_session()
        params_to_state()

    # If we are inited *or* this is a continued session,
    # then we want to copy the **allowed** params to the URL
    state_to_params()
