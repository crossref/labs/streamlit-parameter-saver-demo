APP_NAME = "Parameter Saver Demo"

ABOUT = """
This Streamlit app is designed to demonstrate how you can save parameters in the URL and then restore them when the user returns to the app.

This allows users to send copy and paste links to each other that will restore the app to the state they were in when they copied the link.

Note that the **A Secret** password input field is not saved in the URL because you should never save secrets in the URL.

It is omitted because it is not included in `ALLOWED_PARAMS` in `crutils/settings.py`.

"""

DEFAULT_STATE = {
    "inited": True,
    "color": "red",
    "size": "m",
    "letters": [],
    "password": None,
    "visible": False,
}

ALLOWED_PARAMS = ["color", "size", "letters", "visible"]
