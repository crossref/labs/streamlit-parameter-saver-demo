### Streamlit Parameter Saver Demo

This Streamlit app is designed to demonstrate how you can save parameters in the URL and then restore them when the user returns to the app.

Updated: 2024-02-20 to handle the awkward way in which Streamlit purges session_state widget key variables when they are no longer visible (e.g. in multi-page apps, etc.)

This allows users to send copy and paste links to each other that will restore the app to the state they were in when they copied the link.

Note that the **A Secret** password input field is not saved in the URL because you should never save secrets in the URL.

It is omitted because it is not included in `ALLOWED_PARAMS` in `crutils/settings.py`.

To see how this works:

- change a few of the settings on the main screen
- see how that changes the URL
- copy the URL
- open a new window and then paste the copied URL into the URL-bar
- go to other pages and see that the values persist
- copy the URL from a sub-page
- open a new window and then paste the copied URL into the URL-bar

It should create a new window with controls set to the same values as the previous window (except for the `A Secret` field)

Finally, note that the functionality for managing this is kept in `crutils/sessions.py`
