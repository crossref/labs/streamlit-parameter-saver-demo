import streamlit as st
from streamlit_parameter_saver_demo.settings import APP_NAME
from streamlit_parameter_saver_demo.sessions import (
    init,
    restore_widget_value,
    persist_widget_value,
)

PAGE_NAME = __file__.split("/")[-1].replace(".py", "").capitalize()


def display_main():
    st.title(f"{APP_NAME} : {PAGE_NAME}")
    st.header(f"{APP_NAME} : {PAGE_NAME}")
    st.write(open("README.md").read())
    restore_widget_value("color")
    st.selectbox(
        "Color",
        ["red", "purple", "orange"],
        key="_color",
        on_change=persist_widget_value,
        args=["color"],
    )
    restore_widget_value("size")
    st.selectbox(
        "Size",
        ["s", "m", "l"],
        key="_size",
        on_change=persist_widget_value,
        args=["size"],
    )
    restore_widget_value("letters")
    st.multiselect(
        "Letters",
        ["a", "b", "c"],
        key="_letters",
        default=None,
        on_change=persist_widget_value,
        args=["letters"],
    )
    restore_widget_value("visible")
    st.checkbox(
        "Visible", key="_visible", on_change=persist_widget_value, args=["visible"]
    )

    restore_widget_value("password")
    st.text_input(
        "A Secret",
        type="password",
        key="_password",
        on_change=persist_widget_value,
        args=["password"],
    )


init()
# # And then we go about our main business
display_main()
