import streamlit as st
from streamlit_parameter_saver_demo.sessions import init
from streamlit_parameter_saver_demo.settings import APP_NAME, ALLOWED_PARAMS

PAGE_NAME = __file__.split("/")[-1].replace(".py", "").capitalize()

def display_main():
    st.title(f"{APP_NAME} : {PAGE_NAME}")
    st.header(f"{APP_NAME} : {PAGE_NAME}")
    st.subheader("Session State")
    for k in ALLOWED_PARAMS:
        st.write(f"{k}: {st.session_state.get(k,"[not set]")}")


init()
# # And then we go about our main business
display_main()
